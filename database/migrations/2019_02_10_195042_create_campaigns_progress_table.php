<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns_progress', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('campaign_id');
            $table->integer('set_id');
            $table->integer('lvl_active')->default(1);
            $table->integer('lives')->default(3);
            $table->integer('next_life')->default(25000);
            $table->integer('scores')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaings_progress');
    }
}
