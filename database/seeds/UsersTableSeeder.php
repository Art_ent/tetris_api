<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Artemis',
                'email' => 'artentrery@gmail.com',
                'password' => bcrypt('111111'),
                'fio' => 'Yuri Yurchenko',
                'status' => 'Life is beautiful',
                'avatar' => NULL,
                'created_at' => '2018-03-01 13:24:28',
                'updated_at' => '2018-03-01 13:24:28',
            ]
        ]);
    }
}
