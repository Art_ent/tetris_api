<?php

use Illuminate\Database\Seeder;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaigns')->insert([
            [
                'id' => 1,
                'set' => 1,
                'level' => '{}',
                'order' => 1,
            ],
            [
                'id' => 2,
                'set' => 1,
                'level' => '{}',
                'order' => 2,
            ],
            [
                'id' => 3,
                'set' => 1,
                'level' => '{}',
                'order' => 3,
            ],
            [
                'id' => 4,
                'set' => 1,
                'level' => '{}',
                'order' => 4,
            ]
        ]);
    }
}
