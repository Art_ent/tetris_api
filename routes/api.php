<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Tetris API
Route::group(['middleware' => ['auth:api']], function () {
    // User request
    Route::get('/user',         'ApiUserController@user');
    Route::post('/user/update', 'ApiUserController@update');
    Route::post('/user/logout', 'ApiUserController@logout');

    // Quick game request
    Route::post('/quickgame',          'ApiQuickGameController@newGame');
    Route::post('/quickgame/gameover', 'ApiQuickGameController@saveGame');
    Route::get('/quickgame/scores',   'ApiQuickGameController@leaders');

    // Campaign
    Route::post('/campaign',   'ApiCampaignsController@newCampaign');
    Route::post('/campaign/victory',   'ApiCampaignsController@victory');
    Route::post('/campaign/defeat',   'ApiCampaignsController@defeat');
});
