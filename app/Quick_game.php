<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quick_game extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','game_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
