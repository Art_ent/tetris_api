<?php

namespace App\Http\Controllers;

use App\Quick_game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiQuickGameController extends Controller
{
    /**
     * Register a new Game
     *
     * @param array $request
     * @return JSON message
     */
    public function newGame(Request $request)
    {
        $old_game = Quick_game::where( 'user_id', '=', $request->user()->id )->first();
		$record = Quick_game::orderBy('record','DESC')->first();

        if( $old_game === null ) {
            $game = Quick_game::create([
                'user_id' => $request->user()->id,
                'game_id' => uniqid()
            ]);

        } else {
            $game = $old_game;
            $game->scores = null;
            $game->game_id = uniqid();
            $game->update();
        }

		$game->leader = $record->record;
		
        return response()->json([
            'status' => 200,
            'message' => 'New game.',
            'data' => $game
        ], 201);
    }

    /**
     * Save results of game
     *
     * @param array $request
     * @return JSON message
     */
    public function saveGame(Request $request)
    {
        // First check data for
        $validator = Validator::make($request->all(), [
            'game_id' => 'required|string',
            'scores'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'status' => 400,
                'error' => $errors->first()
            ], 201);
        } else {
            $game = Quick_game::where( [ ['user_id', '=', $request->user()->id],
                                         ['game_id', '=', $request->game_id ],
                                         ['scores', '=', NULL ] ] )->first();
            if($game === null)
                return response()->json([
                    'status' => 400,
                    'message' => 'Session of the game is outdated'
                ], 201);


            $game->scores = $request->scores;
            $game->record = ( $request->scores < $game->record ) ? $game->record : $request->scores;
            $game->update();

            return response()->json([
                'status' => 200,
                'message' => 'Your result has been saved.'
            ], 201);
        }
    }

    /**
     * Show leaders of game
     *
     * @param array $request
     * @return JSON message
     */
    public function leaders(Request $request)
    {
        $data = Quick_game::with('user')->orderBy('record','DESC')->paginate(10);

        return response()->json([
            'status' => 200,
            'message' => 'Table of the best;)',
            'data' => $data
        ], 201);
    }
}
