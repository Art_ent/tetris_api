<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Mail\Register;

use Illuminate\Http\Request;

class ApiUserController extends Controller
{
    /**
     * Register a new user
     *
     * @param array $request
     * @return \App\User
     */
    public function register(Request $request)
    {
        // First check data for
        $validator = Validator::make($request->all(), [
            'email'    => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'status' => 400,
				'error' => $errors->first()				
            ], 201);
        } else {
            $user = User::create([
                'name' => 'Player',
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);

            $m_user = new User;
            $m_user->name = $user['name'];
            $m_user->email = $user['email'];
            $m_user->password = $request['password'];

            Mail::to($m_user)->send(new Register($m_user));

            return response()->json([
                'status' => 200,
				'message' => 'You registered! Authorization...',
                'user' => $user				
            ], 201);
        }
    }

    /**
     * Logout current user
     *
     * @param array $request
     * @return JSON message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->delete();

        return response()->json([
            'status' => 200,
			'message' => 'User has been logout!',
        ], 200);
    }

    /**
     * Logout current user
     *
     * @param array $request
     * @return JSON message
     */
    public function user(Request $request)
    {
        return response()->json([
			'status' => 200,
            'message' => 'Here your info:)',
            'user' => $request->user()
        ], 200);
    }

    /**
     * Update user
     *
     * @param Request
     * @return \App\User
     */
    public function update(Request $request)
    {
        // First check data for
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|min:3|max:255',
            'fio'       => 'required|string|min:2|max:255',
            'status'    => 'required|string|min:3|max:255',
            'avatar'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
		
        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'status' => 400,
                'error' => $errors->first()
            ], 201);
        } else {
            $request->user()->name  = request('name');
            $request->user()->fio   = request('fio');
            $request->user()->status = request('status');

            // Upload image
            if(request('avatar') !== NULL) {
                if ($request->user()->avatar) {
                    $filename = base_path().$request->user()->avatar;
                    File::delete($filename);
                }
                $imageName =  time().'.'.$request->avatar->getClientOriginalExtension();
                $request->avatar->move(public_path('avatar'), $imageName);

                $request->user()->avatar = '/public/avatar/'.$imageName;
            }

            $request->user()->save();

            return response()->json([
                'status' => 200,
                'message' => 'Your profile has been updated!',
                'user' => $request->user()
            ], 201);
        }
    }
}
