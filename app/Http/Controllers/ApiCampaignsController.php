<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\Campaign_progress;
use Illuminate\Support\Facades\Validator;

class ApiCampaignsController extends Controller
{
    /**
     * Start a new Campaign
     *
     * @param array $request
     * @return JSON message
     */
    public function newCampaign(Request $request)
    {
       $start = Campaign::where( [[ 'set', '=', $request->campaign ],
                                  [ 'order', '=', 1 ]] )->first();
	  								  
	   if( $start === null )
		    return response()->json([
				'status' => 200,
				'message' => 'Copmany does not exists.',
			], 201);
	   								  
       $old_campaign = Campaign_progress::where( 'user_id', '=', $request->user()->id )->first();

       $start->game_id = uniqid();
	   $start->lives = 3;

        if( $old_campaign === null ) {
            Campaign_progress::create([
                'user_id' => $request->user()->id,
                'campaign_id' => $start->game_id ,
                'set_id' => 1
            ]);

        } else {
            $old_campaign->lives = 3;
            $old_campaign->scores = 0;
            $old_campaign->next_life = 25000;
            $old_campaign->lvl_active = 1;
            $old_campaign->campaign_id = $start->game_id ;
            $old_campaign->update();
        }

        return response()->json([
            'status' => 200,
            'message' => 'Ready to start?',
            'data' => $start
        ], 201);
    }

    /**
     * Calculate scores and get next lvl
     *
     * @param array $request
     * @return JSON message
     */
    public function victory(Request $request)
    {
        // First check data for
        $validator = Validator::make($request->all(), [
            'game_id' => 'required|string',
            'scores'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'status' => 400,
                'error' => $errors->first()
            ], 201);
        } else {
            $campaign_progress = Campaign_progress::where( 'user_id', '=', $request->user()->id )->first();

            if($campaign_progress === null)
                return response()->json([
                    'status' => 400,
                    'message' => 'Session of the game is outdated'
                ], 201);

            $campaign_progress->lvl_active += 1;
            $campaign = Campaign::where( [[ 'set', '=', 1 ],
                                          [ 'order', '=', $campaign_progress->lvl_active ]])->first();

            if($campaign !== null) {
                $campaign_progress->scores += $request->scores;

                while( $campaign_progress->scores > $campaign_progress->next_life ) {
                    $campaign_progress->lives += 1;
                    $campaign_progress->next_life *= 2;
                }

                $campaign_progress->update();
				
				$campaign->lives = $campaign_progress->lives;

                return response()->json([
                    'status' => 200,
                    'message' => 'Start next level?',
                    'data' => $campaign
                ], 201);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'Congratulation you finished all levels. ;)'
                ], 201);
            }
        }
    }

    /**
     * Reload lvl and decrement lives
     *
     * @param array $request
     * @return JSON message
     */
    public function defeat(Request $request)
    {
        // First check data for
        $validator = Validator::make($request->all(), [
            'game_id' => 'required|string',
            'scores'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'status' => 400,
                'error' => $errors->first()
            ], 201);
        } else {
            $campaign_progress = Campaign_progress::where( 'user_id', '=', $request->user()->id )->first();

            if($campaign_progress === null)
                return response()->json([
                    'status' => 400,
                    'message' => 'Session of the game is outdated'
                ], 201);

            $campaign = Campaign::where( [[ 'set', '=', 1 ],
										  [ 'order', '=', $campaign_progress->lvl_active ]])->first();

            if($campaign_progress->lives !== 0) {
                $campaign_progress->lives -= 1;
                $campaign_progress->scores += $request->scores;

                while( $campaign_progress->scores > $campaign_progress->next_life ) {
                    $campaign_progress->lives += 1;
                    $campaign_progress->next_life *= 2;
                }

                $campaign_progress->update();
				
				$campaign->lives = $campaign_progress->lives;

                return response()->json([
                    'status' => 200,
                    'message' => 'Try one more time?',
                    'data' => $campaign
                ], 201);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'Game over!!! ;('
                ], 201);
            }
        }
    }
}
