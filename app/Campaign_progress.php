<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign_progress extends Model
{
    public $timestamps = false;
    protected $table = 'campaigns_progress';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','campaign_id','set_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
